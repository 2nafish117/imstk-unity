========
Releases
========
2023-oct v2.0
    - iMSTK Version 7.0
    - Unity 2021.3
    - Refactored PbdObject to `Deformable` and `Rigid`
    - Added Collision Editing to `Deformable` and `Rigid`
    - Decoupled Simulation manager from Unity fixed update
    - Enhanced Support for Grasping w/ Unity Editors
    - Support for anisotropic deformables
    - Support for constraining deformables through Unity meshes
    - Support for suturing
    - Better handling of disabled components
    - Various Bug fixes

2022-jul v1.0 First release (amongst others): 
    - iMSTK Version 6.0
    - Support to import VTK and other mesh formats (e.g. vtk, vtu, stl, ply, veg, …)
    - Deformable & Rigid Body Models to be used in Unity
    - Custom Editors for iMSTK Behaviors
    - Helper Scripts to create simple dynamic objects

